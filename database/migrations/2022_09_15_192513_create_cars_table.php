<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('model_id')->nullable();
            $table->unsignedInteger('year');
            $table->unsignedInteger('car_mileage');
            $table->string('color', 50);
            $table->unsignedInteger('price');
            $table->string('car_img', 255)->nullable();

// idx
            $table->index('brand_id', 'car_brand_idx');
            $table->index('model_id', 'car_model_idx');

///fk
            $table->foreign('brand_id', 'car_brand_fk')->on('brands')->references('id');
            $table->foreign('model_id', 'car_model_fk')->on('models')->references('id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
};
