<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\V1\CarModelController;
use \App\Http\Controllers\Api\V1\BrandController;
use \App\Http\Controllers\Api\V1\CarController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/car-model', [CarModelController::class, 'getCarModel']);
Route::post('/car-model/all', [CarModelController::class, 'getAllCarModel']);

Route::post('/car-brand', [BrandController::class, 'getBrand']);
Route::post('/car-brand/all', [BrandController::class, 'getAllBrand']);

Route::post('/cars', [CarController::class, 'getCars']);
Route::post('/cars/all', [CarController::class, 'getAllCars']);

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/
