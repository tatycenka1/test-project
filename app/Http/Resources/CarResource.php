<?php

namespace App\Http\Resources;

use App\Models\Brand;
use App\Models\CarModel;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'model' => CarModel::find($this->model_id)->name,
            'brand' => Brand::find($this->brand_id)->name,
            'year' => $this->year,
            'car_mileage' => $this->car_mileage,
            'price' => $this->price
        ];
    }
}
