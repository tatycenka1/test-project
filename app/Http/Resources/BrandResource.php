<?php

namespace App\Http\Resources;

use App\Models\CarModel;
use Illuminate\Http\Resources\Json\JsonResource;

class BrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //   return parent::toArray($request);
        return [
            'name' => $this->name,
            'model' => CarModel::find($this->model_id)->name,
        ];
    }
}
//PostResource::collection($this->posts),
