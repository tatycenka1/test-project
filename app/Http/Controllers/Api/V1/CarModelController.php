<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CarModelCollection;
use App\Http\Resources\CarModelResource;
use App\Models\CarModel;
use Illuminate\Http\Request;

class CarModelController extends Controller
{
    public function getCarModel() {
      return  new CarModelResource(CarModel::orderByDesc('name')->first());
    }

    public function getAllCarModel() {
        return  new CarModelCollection(CarModel::all());
    }
}
