<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CarCollection;
use App\Http\Resources\CarResource;
use App\Models\Car;
use App\Models\CarModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CarController extends Controller
{

    public function getCars()
    {
        return new CarResource(DB::table('cars')
            ->join('models', 'cars.model_id', '=', 'models.id')
            ->join('brands', 'cars.brand_id', '=', 'brands.id')
            ->first());
        //   return new CarResource(Car::orderBy('brand_id', 'asc')->first());
    }

    public function getAllCars(Request $request)
    {
        $url = $request->getRequestUri();
        $query = parse_url($url);

        parse_str($query['query'], $result);
      //  dd($result['column']);
        return new CarCollection(DB::table('cars')
            ->orderByDesc($result['column'])->get());
    }
}

