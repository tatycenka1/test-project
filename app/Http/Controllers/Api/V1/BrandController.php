<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\BrandCollection;
use App\Http\Resources\BrandResource;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function getBrand() {
        return  new BrandResource(Brand::orderByDesc('name')->first());
    }

    public function getAllBrand() {
        return  new BrandCollection(Brand::all());
    }
}
