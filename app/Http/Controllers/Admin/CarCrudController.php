<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CarModelRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Storage;

/**
 * Class CarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CarCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Car::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/car');
        CRUD::setEntityNameStrings('car', 'cars');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('brand_id')
            ->type('select')
            ->entity('getBrand')
            ->attribute('name')
            ->model('App\Models\Brand');

        CRUD::column('model_id')
            ->type('select')
            ->entity('getModel')
            ->attribute('name')
            ->model('App\Models\CarModel');

        CRUD::column('year');
        CRUD::column('car_mileage');
        CRUD::column('color');
        CRUD::column('price');
        CRUD::column('car_img')
            ->type('image')
            ->prefix(asset('storage') . '/');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CarModelRequest::class);

        CRUD::field('brand_id')
            ->type('select')
            ->model('App\Models\Brand')
            ->attribute('name');

        CRUD::field('model_id')
            ->type('select')
            ->model('App\Models\CarModel')
            ->attribute('name');

        CRUD::field('year')
            ->type('date');

        CRUD::field('car_mileage');

        CRUD::field('color')
            ->type('color');

        CRUD::field('price');

        CRUD::field('car_img')
            ->type('upload')
            ->upload(true);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $url = asset('storage') . '/';
        CRUD::column('car_img')
            ->type('image')
            ->prefix($url)
            ->width('640px')
            ->height('480px');
        $this->setupListOperation();
    }
}
