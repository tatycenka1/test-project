<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'cars';
    protected  $guarded = false;

    public function getModel() {
        return $this->belongsTo('App\Models\CarModel', 'model_id', "id");
    }

    public function getBrand() {
        return $this->belongsTo('App\Models\Brand', 'brand_id', "id");
    }

    public function setCarImgAttribute($value)
    {
        $attribute_name = "car_img";
        $disk = "public";
        $destination_path = "/";
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
}
