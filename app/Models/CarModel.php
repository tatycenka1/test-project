<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'models';
    protected  $guarded = false;


    public function getModel() {
        return $this->belongsTo('App\Models\CarModel', 'model_id', "id");
    }

    public function getBrand() {
        return $this->belongsTo('App\Models\Brand', 'brand_id', "id");
    }
}
